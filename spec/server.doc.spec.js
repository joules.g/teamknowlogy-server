/**
 * Doc Endpoint Tests
 */
require('dotenv').config()

const axios = require('axios')

const mutationUrl = () => `http://localhost:${process.env.SERVER_PORT}/doc/`

describe('Server Endpoint Stats', () => {
  it('Stats', (done) => {
    axios({
      url: mutationUrl(),
      method: 'get',
      timeout: 500
    }).then((res) => {
      expect(res.status).toBe(200)
    }).catch((err) => {
      console.log(err)
      expect(undefined).toBeDefined()
    }).finally(() => {
      done()
    })
  })
})
