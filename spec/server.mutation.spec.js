/**
 * Mutations Endpoint Tests
 */
require('dotenv').config()

const readJSONFile = require('../src/helper/file').readJSONFile
const axios = require('axios')

const mockDNASequences = readJSONFile('spec/resources/dna.sequences.json')

const mutationUrl = () => `http://localhost:${process.env.SERVER_PORT}/mutation/`

describe('Server Endpoint Mutation', () => {
  it('Set Sequences', (done) => {
    const myPromises = []
    mockDNASequences.map((seq) => {
      const instance = axios({
        url: mutationUrl(),
        method: 'post',
        timeout: 100,
        data: { adn: seq.input },
        validateStatus: function (status) {
          return [200, 400, 403].indexOf(status) !== -1
        }
      })
      myPromises.push(instance)
    })
    Promise.all(myPromises).then((results) => {
      results.map((res, i) => {
        let expectedStatus = 403
        if (mockDNASequences[i].expected.isValidDNA === false) {
          expectedStatus = 400
        } else if (mockDNASequences[i].expected.hasMutations === true) {
          expectedStatus = 200
        }
        expect(res.status).toBe(expectedStatus)
      })
    }).catch((errs) => {
      console.log(errs)
      expect(undefined).toBeDefined()
    }).finally(() => {
      done()
    })
  })
})
