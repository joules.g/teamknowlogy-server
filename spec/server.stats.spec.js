/**
 * Mutations Endpoint Tests
 */
require('dotenv').config()

const axios = require('axios')

const mutationUrl = () => `http://localhost:${process.env.SERVER_PORT}/stats/`

describe('Server Endpoint Stats', () => {
  it('Stats', (done) => {
    axios({
      url: mutationUrl(),
      method: 'get',
      timeout: 100
    }).then((res) => {
      expect(res.status).toBe(200)
      expect(res.data.ratio).toBeDefined()
      expect(res.data.count_mutations).toBeDefined()
      expect(res.data.count_no_mutation).toBeDefined()
    }).catch((err) => {
      console.log(err)
      expect(undefined).toBeDefined()
    }).finally(() => {
      done()
    })
  })
})
