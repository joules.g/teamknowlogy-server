const readJSONFile = require('../src/helper/file').readJSONFile

describe('File Helpers', () => {
  it('readJSONFile Existent File', () => {
    const result = readJSONFile('spec/resources/mock.json')
    expect(result).toEqual({
      prop1: 1,
      prop2: 'string'
    })
  })
  it('readJSONFile Non Existent File', () => {
    const filename = 'spec/resources/nonexistentmock.json'
    expect(function () {
      readJSONFile(filename)
    }).toThrow(
      new Error(`File (${filename}) not found`)
    )
  })
})
