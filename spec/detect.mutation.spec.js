const readJSONFile = require('../src/helper/file').readJSONFile
const DetectMutation = require('../src/mutation/detect.mutation')

const mockDNASequences = readJSONFile('spec/resources/dna.sequences.json')

describe('Detect Mutation', () => {
  it('Set Sequences', () => {
    const mutations = new DetectMutation()
    mockDNASequences.map((seq) => {
      mutations.setDNA(seq.input)
      expect(mutations.dna.length).toBe(seq.input.length)
    })
  })
  it('Validate Sequences', () => {
    const mutations = new DetectMutation()
    mockDNASequences.map((seq) => {
      mutations.setDNA(seq.input)
      expect(mutations.checkValidDNA()).toBe(seq.expected.isValidDNA)
      expect(mutations.validationErrors.length).toBe(seq.expected.validationErrorsLength)
    })
  })
  it('Detect Mutations', () => {
    const mutations = new DetectMutation()
    mockDNASequences.map((seq) => {
      mutations.setDNA(seq.input)
      // console.log(mutations.extendedDNATypes)
      expect(mutations.checkMutations()).toBe(seq.expected.hasMutations)
      expect(mutations.mutations.length).toBe(seq.expected.mutationsLength)
      expect(mutations.hasMutation()).toBe(seq.expected.hasMutations)
      expect(mutations.mutations.length).toBe(seq.expected.mutationsLength)
    })
  })
  it('Get DNA Register', () => {
    const mutations = new DetectMutation()
    mockDNASequences.map((seq) => {
      mutations.setDNA(seq.input)
      expect(
        JSON.stringify(mutations.getDNARegister())
      ).toBe(
        JSON.stringify(seq.expected.dnaRegister)
      )
    })
  })
})
