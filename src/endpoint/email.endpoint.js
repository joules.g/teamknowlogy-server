/**
 * Stats Endpoint
 */
const errorRequest = (res) => {
  res.status(500).send('')
}

const getStats = (cs) => {
  const collection = cs.db.collection(process.env.MONGO_COLLECTION)
  return Promise.all([
    collection.find({ hasMutation: true }).count(),
    collection.find({ hasMutation: false }).count()
  ])
}

module.exports = (req, res) => {
  getStats(req.app.get('dbc')).then((counts) => {
    res.send({
      count_mutations: counts[0],
      count_no_mutation: counts[1],
      ratio: counts[0] / counts[1]
    })
  }).catch((_err) => {
    errorRequest()
  })
}
