/**
 * Mutation endpoint
 */
const errorRequest = (res) => {
  res.status(500).send('')
}

const forbiddenRequest = (res) => {
  res.status(403).send('')
}

const okRequest = (res) => {
  res.status(200).send('')
}

const upsertDNARow = (cs, dnaRegister) => {
  const collection = cs.db.collection(process.env.MONGO_COLLECTION)
  return collection.updateOne(
    { _id: dnaRegister.hash },
    { $set: dnaRegister },
    { upsert: true }
  )
}

module.exports = (req, res) => {
  const dnaRegister = req.body.mutations.getDNARegister()
  upsertDNARow(
    req.app.get('dbc'),
    req.body.mutations.getDNARegister()
  ).then((result) => {
    if (!dnaRegister.hasMutation) {
      forbiddenRequest(res)
    } else {
      okRequest(res)
    }
  }).catch((_err) => {
    errorRequest()
  })
}
