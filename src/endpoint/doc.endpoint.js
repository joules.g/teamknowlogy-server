/**
 * Docs Endpoint
 */
const path = require('path')
const raml2html = require('raml2html')
const configWithDefaultTheme = raml2html.getConfigForTheme()

const errorRequest = (res) => {
  res.status(500).send('')
}

module.exports = (req, res) => {
  raml2html.render(
    path.join(req.rootPath, process.env.RAML_SOURCE),
    configWithDefaultTheme
  ).then(function (result) {
    res.status(200).send(result)
  }, function (_err) {
    console.log(_err)
    errorRequest(res)
  })
}
