const md5 = require('md5')

/**
 * Check Mutation Detection
 * @type {RegExp}
 */
/**
 * Valid Protein Chain
 * @type {RegExp}
 */
const VALID_PROTEIN_CHAIN = new RegExp('[ATCG]{6}', 'i')

/**
 * Mutation Protein Chain
 * @type {RegExp}
 */
const MUTATION_PROTEIN_CHAIN = new RegExp('A{4}|T{4}|C{4}|G{4}', 'i')

class DetectMutation {
  /**
   * Detect Mutation
   * @param {Array} dna DNA Secuence
   */
  constructor (dna = []) {
    this.setDNA(dna)
  }

  /**
   * Update diagonals with given
   * dna, position idx and diagonal array
   * -2 -> 2 for getting principal diagonals. idx + i
   * -2 -> 2 for getting back diagonals. length -1 - i - idx
   * @param  {Array} diagonals  diagonal arrays
   * @param  {number} idx       position
   * @param  {string} pc        protein chain
   */
  updateDiagonals (diagonals, idx, pc) {
    for (let i = -2, j = 0, k = 5; i < 3; i++, j++, k++) {
      const back = pc.length - 1 - i - idx
      const pos = i + idx
      diagonals[j] += pos < pc.length && pos >= 0
        ? pc.charAt(pos)
        : ''
      diagonals[k] += back < pc.length && back >= 0
        ? pc.charAt(back)
        : ''
    }
  }

  /**
   * Set DNA
   * Process DNA array to store and
   * get Vertical and Diagonal extra strings
   * for check mutations purposes
   * All string mapped and stored in extendedDNATypes
   * @param {Array} dna DNA Sequence
   */
  setDNA (dna) {
    this.dna = dna
    const vertical = Array.from({ length: this.dna.length }, (_) => '')
    const diagonals = Array.from({ length: 10 }, (_) => '')
    this.dna.map((pc, i) => {
      for (let j = 0; j < pc.length; j++) {
        vertical[j] += `${pc.charAt(j)}`
      }
      this.updateDiagonals(diagonals, i, pc)
    })
    this.extendedDNATypes = {
      horizontal: this.dna,
      vertical: vertical,
      diagonal: diagonals
    }
  }

  /**
   * Check Valid DNA
   * @return {boolean} Result of validation
   */
  checkValidDNA () {
    let result = true
    this.validationErrors = []
    this.dna.map((pc, i) => {
      if (VALID_PROTEIN_CHAIN.test(pc) === false) {
        result = false
        this.validationErrors.push(`Chain ${i} is not valid: ${pc}`)
      }
    })
    return result
  }

  /**
   * Check Mutations
   * If horizontal, vertical or diagonal 4 consecutive letters return true
   * iterate over rows and stat each vertical and diagonal
   * merge into array then check for 4 consecutive letters
   * @return {boolean} If given Sequence has Mutations
   */
  checkMutations () {
    this.mutations = []
    for (const prop in this.extendedDNATypes) {
      if (typeof this.extendedDNATypes[prop] !== 'undefined') {
        this.extendedDNATypes[prop].map((pc) => {
          if (MUTATION_PROTEIN_CHAIN.test(pc) === true) {
            this.mutations.push(`Chain ${prop} has a consecutive: ${pc}`)
          }
        })
      }
    }
    return this.mutations.length > 1
  }

  /**
   * Has Mutation
   * @return {Boolean} An alias of Check Mutation
   */
  hasMutation () {
    return this.checkMutations()
  }

  /**
   * Get DNA Object
   * @return {Object} DNA Object
   */
  getDNARegister () {
    return {
      hash: md5(JSON.stringify(this.dna)),
      dna: this.dna,
      validDNA: this.checkValidDNA(),
      hasMutation: this.checkMutations()
    }
  }
}

module.exports = DetectMutation
