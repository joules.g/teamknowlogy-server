/**
 * Server APP
 */

/**
* Enviroment configurations!
*/
require('dotenv').config()

const path = require('path')
const express = require('express')
const app = express()
const rootPath = path.join(__dirname, '../')

/**
 * ROOT Path middleware
 */
app.use(
  (req, _res, next) => {
    req.rootPath = rootPath
    next()
  }
)

/**
 * JSON Body Parser middleware
 */
app.use(
  require('./middleware/body.parser.middleware')
)

/**
 * DB middleware
 */
app.use(
  require('./middleware/db.connection.middleware')
)

/**
 * Mutation Endpoint
 */
app.post(
  '/mutation(/)?',
  require('./middleware/mutation.payload.middleware'),
  require('./endpoint/mutation.endpoint')
)

/**
 * Stat Endpoint
 */
app.get(
  '/stats(/)?', require('./endpoint/stats.endpoint')
)

/**
 * Doc Endpoint
 */
app.get(
  '/doc(/)?', require('./endpoint/doc.endpoint')
)

/**
 * Not found endpoints
 */
app.all('*', (_req, res) => {
  res.status(404).send('')
})

app.listen(process.env.SERVER_PORT, '0.0.0.0', () => {
  console.log(`Server tunning on port ${process.env.SERVER_PORT}`)
})
