
/**
 * Store Client
 */
class ClientStore {
  /**
   * Constructor
   * @param {MongoClient} client Mongo client
   * @param {string} db     DB name
   */
  constructor (client, db) {
    this.client = client
    this.db = client.db(db)
  }
}

module.exports = ClientStore
