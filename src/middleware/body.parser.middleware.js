/**
 * JSON Body parser
 *
 */
const bodyParser = require('body-parser')

const addRawBody = (req, _res, buf, _encoding) => {
  req.rawBody = buf.toString()
}

const badRequest = (res) => {
  res.status(400).send('')
}

module.exports = function (req, res, next) {
  bodyParser.json({
    verify: addRawBody
  })(req, res, (err) => {
    try {
      if (err) {
        throw new Error('Body format')
      }
    } catch (e) {
      badRequest(res)
    }
    next()
  })
}
