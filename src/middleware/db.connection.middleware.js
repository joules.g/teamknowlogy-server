const MongoClient = require('mongodb').MongoClient
const ClientStore = require('../store/client.store')

const url = `mongodb://${process.env.MONGO_SERVER}/`

module.exports = function (req, _res, next) {
  if (req.app.get('dbc')) {
    next()
  } else {
    new Promise((resolve, reject) => {
      MongoClient.connect(url, function (err, db) {
        if (err) {
          reject(err)
        }
        resolve(db)
      })
    }).then((client) => {
      const cs = new ClientStore(client, process.env.MONGO_DB)
      req.app.set('dbc', cs)
      next()
    }).catch((err) => {
      console.log('Error on DB connection', err)
    })
  }
}
