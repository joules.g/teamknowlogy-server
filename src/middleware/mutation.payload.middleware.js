/**
 * Mutation Payload middleware
 */
const DetectMutation = require('../mutation/detect.mutation')

const badRequest = (res) => {
  res.status(400).send('')
}

module.exports = function (req, res, next) {
  try {
    if (typeof req.body.adn !== 'object' ||
      typeof req.body.adn.filter !== 'function') {
      throw new Error('Payload formatting')
    }
    const mutations = new DetectMutation(req.body.adn)
    if (!mutations.checkValidDNA()) {
      throw new Error('Not valid DNA')
    }
    req.body.mutations = mutations
    next()
  } catch (e) {
    badRequest(res)
  }
}
