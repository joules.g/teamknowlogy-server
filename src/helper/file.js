
const fs = require('fs')

/**
 * Read a file and parse content to Object
 * @param  {String} filename File name
 * @return {String}          Object parsed from file
 */
const readJSONFile = (filename) => {
  try {
    return JSON.parse(
      fs.readFileSync(filename)
    )
  } catch (err) {
    throw new Error(`File (${filename}) not found`)
  }
}

exports.readJSONFile = readJSONFile
