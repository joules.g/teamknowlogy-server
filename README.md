# Teamknology Challenge

### Prerequisites

1. Install Node.js 12+
2. Install MongoDB
3. Copy .env.example to .env and configure ENV variables
4. run ``npm install``


### Development

For development server you only need to run
```SHELL
npm run dev
```

### Test

For run the tests just run and be sure that development server is running
```SHELL
npm run test
```

### Deploy

For more production ready i propose pm2 and nginx as reverse proxy.

Configure nginx changing the port and host where the Express App is running:
```
# nginx.conf
        location / {
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_pass http://localhost:3000;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header Host $host;
                proxy_cache_bypass $http_upgrade;
        }
```

Start PM2 with bundled `ecosystem.config.js` and
configure the number of workers on `.env`:
```SHELL
pm2 start ecosystem.config.js
```

### Purposes

This project is intended to be an API Rest for DNA Mutation detection and statistics. It has three methods:

1. `GET /doc/` Auto discover API
2. `POST /mutation/` Mutation detection method. For more detail call /doc/ endpoint in browser or preferred REST CLIENT
3. `GET /stats/` Statistics of Mutation detection register. For more detail call /doc/ endpoint in browser or preferred REST CLIENT

Any other methods or endpoints will response with 404
