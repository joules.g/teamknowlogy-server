require('dotenv').config()

module.exports = {
  apps: [
    {
      name: 'teamknowlogy-server',
      script: 'src/server.js',
      watch: '.',
      instances: process.env.PM2_WORKERS,
      exec_mode: 'cluster'
    }
  ]
}
